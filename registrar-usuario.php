<?php if(!isset($_SESSION)) 
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">

    </head>

<script type="text/javascript">
    function registrar_cliente(){
    var nombre =document.getElementById('nombre').value;
  var apellido =document.getElementById('apellido').value;
  var email =document.getElementById('email').value;
  var nombre_usuario =document.getElementById('nombre_usuario').value;
    var password =document.getElementById('password').value;
    var telefono =document.getElementById('telefono').value;
    var celular =document.getElementById('celular').value;

    $.post('procesos/agregar-cliente.php', {
              "nombre": nombre,
              "apellido":apellido,         
              "email":email,              
              "nombre_usuario":nombre_usuario,              
              "password":password, 
              "telefono":telefono,
              "celular":celular     
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
            window.location.replace("mi-cuenta.php"); 
          })
   console.log("reg de usuario ok");  
  
}
</script>


    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
         <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php' ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->
		<!-- Register start-->
<section class="register-page section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Crear cuenta</h3>
                <div class="theme-card">
                    <form class="theme-form theme-form-one">
                        <div class="form-row">
                            <div class="col-md-4">
                                <label >Nombres</label>
                                <input type="text" class="form-control w-100"  required="" id="nombre">
                            </div>
                            <div class="col-md-4">
                                <label for="review">Apellidos</label>
                                <input type="text" class="form-control w-100"   required="" id="apellido">
                            </div>
                            <div class="col-md-4">
                                <label >Correo electrónico</label>
                                <input type="text" class="form-control w-100" id="email"  required="">
                            </div>
                        </div>
                        <div class="form-row">
                            
                            <div class="col-md-3">
                                <label >Nombre usuario</label>
                                <input type="text" class="form-control w-100" id="nombre_usuario"  required="">
                            </div>
                            
                            <div class="col-md-3">
                                <label >Contraseña</label>
                                <input type="password" class="form-control w-100" id="password"   >
                            </div>
                            <div class="col-md-3">
                                <label >Teléfono</label>
                                <input type="password" class="form-control w-100" id="telefono"   >
                            </div>
                            <div class="col-md-3">
                                <label >Celular</label>
                                <input type="password" class="form-control w-100" id="celular"   >
                            </div>
                            <button type="button" class="btn btn-theme theme-btn-sm" onclick="registrar_cliente()">crear cuenta</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Register End -->
<hr>
        <!-- footer section 1 -->
        <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
  

        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
        <script src="assets/js/popper.min.js" ></script>
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
