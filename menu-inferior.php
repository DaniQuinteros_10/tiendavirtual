<div class="bg-light-menu">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="menu-classic d-flex col-side-btn">
					<div class="dropdown category-dropdown theme-header-two sm-space-tb">
						<button class="btn transparant" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Categorias<i class="fa fa-bars d-inline-block" aria-hidden="true"></i></span> <i class="fa fa-bars sidebar-bar"></i></button>
						<div class="dropdown-menu category-list header-two" aria-labelledby="dropdownMenuButton">
							<ul id="sub-menu" class="sm pixelstrap sm-vertical home-header">
								<li class="main-menu-hover d-md-none">
									<div  onclick="closebarNav()">
										<div class="sidebar-back text-left">
											<i class="fa fa-angle-left pr-2" aria-hidden="true"></i> Atrás
										</div>
									</div>
								</li>
								<?php
								$query="SELECT nombre,descripcion,estado FROM categoria WHERE estado='habilitado' "; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
								{
									$descripcion=$row['descripcion'];
									$nombre_categoria=$row['nombre'];
								?>
								<li class="main-menu-hover">
									<a href="#" class="menu-title"><?php echo $nombre_categoria;?></a>
								</li>
								<?php } ?>
								
								
							</ul>
						</div>
					</div>
					<div class="lg-left left-space-menu">
						<nav class="theme-home-menu lg-left-0 header-three-style">
							<ul id="main-menu" class="sm pixelstrap home-header sm-horizontal home-1 ">
								<li class="main-menu-hover" onclick="closeNav()">
									<div class="mobile-back text-right">
										Back<i class="fa fa-angle-right pl-2" aria-hidden="true"></i>
									</div>
								</li>
								<li >
									<a href="index.php" >INICIO</a>
								</li>
								<li >
									<a href="productos.php" >PRODUCTOS</a>
								</li>
								<li >
									<a href="nosotros.php" >NOSOTROS</a>
								</li>
								<li >
									<a href="contacto.php" >CONTACTO</a>
								</li>
								
								
								
							</ul>
						</nav>
						<div class="ml-md-4 d-none d-md-block dark-theme-icon">
							<a href="#" title="Call info">
								<i class="flaticon flaticon-start fa-2x align-middle white-icon"></i>
								<span class="call-info">CHATEAR</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>