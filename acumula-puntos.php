<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">

    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
      
        <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php' ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->
        <!-- Header End-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-0">
                    <div class="slide-1 home-slider full-width-slide">
                        <div>
                            <div class="home text-left p-left full-height ">
                                <img src="assets/images/slider/2.jpg" class="img-to-bg" alt="slider">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <!-- <div class="slider-contain">
                                                <div>
                                                    <h3>Welcome to fashion</h3>
                                                    <h1 class="slide-two">men Fashion</h1>
                                                    <h3 class="slide-two sub">Save up to 50% off</h3>
                                                    <a href="#" class="btn btn-theme theme-color">Shop now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- About start-->
<section class="about-page  section-b-space">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-6">
                <h2> Acumula puntos</h2>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,</p>
                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p>
                   <h4>Aun no tienes una cuenta?</h4>
                     <a href="registrar-usuario.php" class="btn btn-theme theme-btn-sm">Crear cuenta</a>
            </div>
            <div class="col-lg-6 login-page">
                <div class="right-login">
                     <h3>Iniciar session</h3>
                   <div class="theme-card">
                    <form class="theme-form-one theme-form mt-0">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control w-100" id="email" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <label for="review">Password</label>
                            <input type="password" class="form-control w-100" id="review" placeholder="Enter your password" required="">
                        </div>
                        <a href="mi-cuenta.php" class="btn btn-theme theme-btn-sm">Login</a>
                    </form>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- About End -->
<!-- pasos para acumular puntos -->
<section class="section-b-space">
    <div class="container">
        <div class="row">
            <h3>Pasos para acumular puntos</h3>
            <div class="col-lg-12">
                <div class="banner-section">
                    <img src="assets/images/about/about%20us.jpg" class="img-fluid  ">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- como canjear -->
<section class="section-b-space">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-8">
                <h3>Como canjear puntos</h3>
                <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,

                    On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish.
                </p>
            </div>
            <div class="col-lg-4">
                <div>
                    
                    <h4>CATALOGO DE REGALOS</h4>
                    <img src="assets/images/cosmetic/product/12.jpg">
                     <a href="#" class="btn btn-theme theme-btn-sm">Descargar catalogo</a>
                </div>
            </div>
        </div>
    </div>
</section>
      

      
       
      
        <!-- footer section 1 -->
       <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->
   
        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
        <!-- <script src="assets/js/popper.min.js" ></script> -->
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
