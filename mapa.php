
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" href="mapa.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <style>
 
      #map {position: float; top: 0; right: 0; bottom: 0; left: 0; width:100%; height:100%;}

    </style>
    <style>
      body, html { height:100%;
        margin:0;
        padding:0;
        width:100%;
        background-color:silver;
        overflow:hidden;
      }
       
      .recuadro {
        border:1px solid black;
        width:49.5%;
        height:49.5%;
        display:inline-block;
        overflow:hidden;
      }
      #uno{
        /*background-color:blue;*/
        }
      #dos{
        /*background-color:green;*/
        }
      #tres{
        /*background-color:yellow;*/
        }
      #cuatro{
        /*background-color:red;*/
        }
      </style>
      <script type="text/javascript">
          function calcular_elevacion(lat, lng) {
            $.post('https://secure.geonames.org/srtm3JSON?lat='+lat+'&lng='+lng+'&username=galwayireland',{

            },function(data){
              console.log('altura', data['srtm3']);
              document.getElementById("altitud").value=data['srtm3'];
            })
            console.log('ok');
          }
        </script>
  </head>
  <body>
    
      <div class='recuadro' id="uno">
         <div id="map">
            <p><a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a></p>
              <script>

                var map = L.map('map').setView([-17.388178, -66.155769], 13);
                L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=APIKEY',{
                  tileSize: 512,
                  zoomOffset: -1,
                  minZoom: 2,
                  attribution: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
                  crossOrigin: true
                }).addTo(map);
                  var popup = L.popup();

                  function onMapClick(e) {
                      popup
                          .setLatLng(e.latlng)
                          .setContent("Cordenadas" + e.latlng.toString())
                          .openOn(map);

                        // damos valores a los inputs  
                          document.getElementById("latitud").value= e.latlng.lat.toString();
                          document.getElementById("longitud").value= e.latlng.lng.toString();
                          calcular_elevacion(e.latlng.lat.toString(), e.latlng.lng.toString());
                          
                  }

                  map.on('click', onMapClick);


                 var rectangle = L.polygon([
                    [-17.388194, -66.155769],
                    [-17.388178, -66.155791],
                    [-17.388178, -66.155747],
                    [-17.388162, -66.155769]
                  ]).addTo(map);


                 var options = {units: 'miles'};

                var along = turf.along(map,50000, options);
             </script>
          </div>
      </div>
      <div class='recuadro' id="dos">
         <div>
           Latitud: <input type="text" id="latitud"><br>
           Longitud: <input type="text" id="longitud"><br>
           Altura: <input type="text" id="altitud"><br>
           <button id="capturar" onclick="">Capturar</button>
           <button id="calcular" onclick="">Calcular</button>
         </div>
      </div>
      <br>
      <div class='recuadro' id="tres">
        
      </div>
      <div class='recuadro' id="cuatro">
        <script type="text/javascript">
           var uno = 17.393332354761455;
           var dos = 17.393527082159416;
           var resta = uno-dos;
           console.log(resta);
        </script>
      </div>
  </body>
</html>
