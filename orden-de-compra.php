<?php if(!isset($_SESSION)) 
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">
    <script type="text/javascript">
    
        function enviar_orden(){
          var nombre= document.getElementById('form_nombre').value;
          var apellido= document.getElementById('form_apellido').value;
          var telefono= document.getElementById('form_telefono').value;
          var email= document.getElementById('form_email').value;
          var latitud= document.getElementById('latitud').value;
          var longitud= document.getElementById('longitud').value;
            $.post('procesos/enviar-orden.php', {
                      "nombre": nombre,
                      "apellido":apellido,
                      "telefono":telefono,
                      "email":email,
                      "latitud":latitud,
                      "longitud":longitud        
                    },function(data) {
                      //console.log('procesamiento finalizado', data);                           
                      if(data='error'){
                        window.locationf="index.php";
                      }
                      else{
                         $("#listacarrito").load("#listacarrito"); 
                         window.location="index.php";
                      }
                  })
           console.log("reg ok");  
            
        }
    </script> 

    <link rel="stylesheet" href="mapa.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <style>
      #map {position: float; top: 0; right: 0; bottom: 0; left: 0; width:100%;     height: 272px;}
    </style>
    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
        <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php';?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php';?>
                <?php include 'menu-inferior.php';?>
            </div>
        </header>
        <!-- Header End-->

        <!-- Checkout start -->
<section>
    <div class="container padding-cls">
        <div class="checkout-page">
            <div class="checkout-form">
                <form class="theme-form">
                    <div class="row checkout-partition">
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h3>Detalles de compra</h3>
                            </div>
                            <div class="row check-out">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">Nombre</div>
                                    <input type="text" name="field-name" value="<?php if (isset($_SESSION['user_id'])) {echo $usuario_nombre;}?>" class="form-control" placeholder="" id="form_nombre">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">Apellidos </div>
                                    <input type="text" name="field-name" value="<?php if (isset($_SESSION['user_id'])) {echo $usuario_apellido;}?>" class="form-control" placeholder="" id="form_apellido">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">Telefono/Cel</div>
                                    <input type="text" name="field-name" value="<?php if (isset($_SESSION['user_id'])) {echo $usuario_telefono." - ".$usuario_celular;}?>"  class="form-control" placeholder="" id="form_telefono">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">Email</div>
                                    <input type="text" name="field-name" value="<?php if (isset($_SESSION['user_id'])) {echo $usuario_email;}?>"  class="form-control" placeholder="" id="form_email">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">Latitud</div>
                                    <div id="map">
                                        <p><a href="https://www.maptiler.com/copyright/" target="_blank">© Mi mapa</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© </a></p>
                                          <script>

                                            var map = L.map('map').setView([-17.388178, -66.155769], 13);
                                            L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=EZP3fjpSESGivJ4thpe8',{
                                              tileSize: 512,
                                              zoomOffset: -1,
                                              minZoom: 2,
                                              attribution: '<a href="" target="_blank">&copy; Mi mapa</a> <a href="" target="_blank">&copy;.</a>',
                                              crossOrigin: true
                                            }).addTo(map);
                                              var popup = L.popup();

                                              function onMapClick(e) {
                                                  popup
                                                      .setLatLng(e.latlng)
                                                      .setContent("Datos " + e.latlng.toString())
                                                      .openOn(map);

                                                    // damos valores a los inputs  
                                                      document.getElementById("latitud").value= e.latlng.lat.toString();
                                                      document.getElementById("longitud").value= e.latlng.lng.toString();
                                                      calcular_elevacion(e.latlng.lat.toString(), e.latlng.lng.toString());
                                                      
                                              }

                                              map.on('click', onMapClick);


                                             var rectangle = L.polygon([
                                                [-17.388194, -66.155769],
                                                [-17.388178, -66.155791],
                                                [-17.388178, -66.155747],
                                                [-17.388162, -66.155769]
                                              ]).addTo(map);


                                             var options = {units: 'miles'};

                                            var along = turf.along(map,50000, options);
                                         </script>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    
                                    <input type="hidden" id="longitud">
                                    <input type="hidden" id="latitud">
                                </div>
                               
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <!--<input type="checkbox" name="shipping-option" id="account-option"> &ensp; <label for="account-option">Create An Account?</label>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-details">
                                <div class="order-box">
                                    <div class="title-box">
                                        <div>Productos <span> Total</span></div>
                                    </div>
                                    <ul class="qty">
                                        <?php 
                                            if (isset($_SESSION['user_id'])) 
                                            { //$subtotal=0;
                                              $query_orden="SELECT producto.imagen,producto.nombre, producto.precio_unidad, producto.precio_cuarta, carrito.cantidad, carrito.tipo_compra FROM carrito join producto on carrito.id_producto=producto.id where carrito.id_usuario= $usuario_id "; 
                                                 $resultado_orden=$conexion->query($query_orden); 
                                                 while($orden=$resultado_orden->fetch_assoc())
                                                    {
                                                        $cantidad_cuarta=$orden['cantidad']*3;
                                                        if($orden['tipo_compra']=='unidad'){
                                                            $total_producto=$orden['cantidad']*$orden['precio_unidad'];
                                                           // $subtotal=$subtotal+$total_producto;
                                                            echo "<li>".$orden['nombre']." × ".$orden['cantidad']." Und. <span>Bs. ".number_format((float)$total_producto, 2, '.', '')."</span></li>";
                                                        }
                                                        else{
                                                            //$total_producto_cuarta=$row1['cantidad']*$row1['precio_cuarta'];
                                                            //$subtotal=$subtotal+$total_producto_cuarta;
                                                                    echo "<li>".$orden['nombre']." × ".$cantidad_cuarta." Und <span>Bs. ".number_format((float)$total_producto_cuarta, 2, '.', '')."</span></li>";
                                                                    }    
                                                    }
                                            }
                                        ?>
   
                                    </ul>
                                    <ul class="sub-total">
                                        <li>Subtotal <span class="count">Bs. <?php if (isset($_SESSION['user_id'])) {echo number_format((float)$subtotal, 2, '.', '');} else{echo "0.00";}?></span></li>
                                        
                                    </ul>

                                    <ul class="total">
                                        <li>Total <span class="count">Bs. <?php if (isset($_SESSION['user_id'])) {echo number_format((float)$subtotal, 2, '.', '');} else{echo "0.00";}?></span></li>
                                    </ul>
                                </div>

                                <div class="payment-box">
                                    <div class="upper-box">
                                        <div class="payment-options">
                                            <ul>
                                                <li>
                                                    <div class="radio-option">
                                                        <input type="radio" name="payment-group" id="payment-1" checked>
                                                        <label for="payment-1">Pagar contra entrega<span class="small-text"></span></label>
                                                    </div>
                                                </li>

                                               
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                       
                                        <button type="button" class="btn btn-theme theme-btn-sm" onclick="enviar_orden()">Enviar Orden </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Checkout End -->
<hr>
        <!-- footer section 1 -->
       <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->
      <!-- Top Add to cart -->
<div id="cart_side" class="add_to_cart top">
    <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>my cart</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCart()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <ul class="cart_product">
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/electronics/product/5.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/fashion1/1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#"><img alt="" class="mr-2" src="assets/images/furniture/5.jpg"></a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4><span>1 x $ 299.00</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5>subtotal : <span>$299.00</span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="carrito.html" class="btn btn-theme theme-color theme-btn-sm">view cart</a>
                        <a href="checkout.html" class="btn btn-theme theme-color theme-btn-sm">checkout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Top Add to cart end-->
       

        <!-- cart start -->
        <div class="add_cart_btm_popup" id="fixed_cart_icon">
            <a href="#" class="fixed_cart">
                <i class="icon-shopping-cart"></i>
            </a>
        </div>
        <!-- cart end -->


        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
    <script src="assets/js/popper.min.js" ></script>
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
