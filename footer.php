<footer>
            <div class="footer-space theme-layout">
                <div class="container">
                    <div class="row footer-theme partition-f">
                        <div class="col-lg-4 col-md-6">
                            <div class="footer-title footer-mobile-title">
                                <h4>Nosotros</h4>
                            </div>
                            <div class="footer-contant">
                                <div class="footer-logo">
                                    <img src="assets/images/logo13.png" alt="logo">
                                </div>
                                <p>Aliquam aliquam dui blandit ante vulputate, id fringilla lorem scelerisque. Vestibulum sit amet tortor lorem. Suspendisse sagittis imperdiet pharetra. Proin neque dui.</p>
                               
                            </div>
                        </div>
                        <div class="col col-lg-4 col-md-6 col-xl-2 offset-xl-1">
                            <div class="sub-title">
                                <div class="footer-title">
                                    <h4>Enlaces directos</h4>
                                    <div class="divider-line"></div>
                                </div>
                                <div class="footer-contant">
                                    <ul>
                                        <li><a href="#">Nosotros</a></li>
                                        <li><a href="#">Contacto</a></li>
                                        <li><a href="#">Terminos &amp; Condiciones</a></li>
                                        <li><a target="_blank" href="Administrador/index.php">Acceder al sistema</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col col-lg-4 col-md-6">
                            <div class="sub-title">
                                <div class="footer-title">
                                    <h4>Contacto</h4>
                                    <div class="divider-line"></div>
                                </div>
                                <div class="footer-contant">
                                    <ul class="contact-list">
                                        <li><i class="fa fa-map-marker"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                        <li><i class="fa fa-phone"></i>Teléfono: 123-456-7898</li>
                                        <li><i class="fa fa-envelope-o"></i>Email: correo@midominio.com</li>
                                        <li><i class="fa fa-fax"></i>WhatsApp: +591 123456</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row social-space-b">
                        <div class="col-md-4">
                            <hr class="social-line">
                        </div>
                        <div class="col-md-4">
                            <div class="footer-social">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <hr class="social-line">
                        </div>
                    </div>
                </div>
            </div>
            <div class="sub-footer theme-light">
                <div class="container">
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="footer-end text-center">
                                <p class="copyright-text">2020</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>