<?php if(!isset($_SESSION)) //!isset($_SESSION)
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">
    <script type="text/javascript">
        function addproducto(id){
            $.post('procesos/addcarrito.php', {
                      "tipo_compra": "unidad",
                      "cantidad": 1,
                      "id_producto": id     
                    },function(data) {
                      //console.log('procesamiento finalizado', data);                           
                      if(data='error'){
                        window.location="carrito.php";
                      }
                      else{
                         //$("#seccioncarrito").load("#seccioncarrito"); 
                         window.location="carrito.php";
                      }
            })
        }
    </script>
    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
        <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
                <?php include 'top-bar.php'; ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->

        <!-- Slider -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 p-0">
                    <div class="slide-1 home-slider full-width-slide">
                        <div>
                            <div class="home text-left p-left full-height ">
                                <img src="assets/images/slider/4_.jpg" class="img-to-bg" alt="slider">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <!-- <div class="slider-contain">
                                                <div>
                                                    <h3>Welcome to fashion</h3>
                                                    <h1 class="slide-two">men Fashion</h1>
                                                    <h3 class="slide-two sub">Save up to 50% off</h3>
                                                    <a href="#" class="btn btn-theme theme-color">Shop now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="home text-left p-left full-height ">
                                <img src="assets/images/slider/2.jpg" class="img-to-bg" alt="slider">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <!-- <div class="slider-contain">
                                                <div>
                                                    <h3>New fashion sale</h3>
                                                    <h1 class="slide-two">Women Fashion</h1>
                                                    <h3 class="slide-two sub">Save up to 40% off</h3>
                                                    <a href="#" class="btn btn-theme theme-color">Shop now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="home text-left p-left full-height ">
                                <img src="assets/images/slider/4.jpg" class="img-to-bg" alt="slider">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <!-- <div class="slider-contain">
                                                <div>
                                                    <h3>All fashion sale</h3>
                                                    <h1 class="slide-two">Latest Fashion</h1>
                                                    <h3 class="slide-two sub">Save up to 30 - 50% off</h3>
                                                    <a href="#" class="btn btn-theme theme-color">Shop now</a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider End -->
        
        <!-- Service -->
        <div class="container">
            <div class="service border-section small-section ">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 service-block">
                        <div class="service-space">
                            <i class="flaticon-truck"></i>
                            <h4>Envios a domicilio</h4>
                            <p>Porque pensamos en su comodidad, contamos con servicio de enetrga a domicilio.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 service-block">
                        <div class="service-space">
                            <i class="flaticon-telemarketer"></i>
                            <h4>Servicio online</h4>
                            <p>Puede realizar su pedido online, o contactarnos directamente por el chat o llamada.</p>
                        </div>
                    </div>
                    
                    <div class="col-sm-6 col-lg-4 service-block">
                        <div class="service-space">
                            <i class="flaticon-credit-card"></i>
                            <h4>Pagos contraentrega</h4>
                            <p>Para su comodidad, los pagos se realizar contraentrega.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service End -->
       
        <!-- Product layout two -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 main-title"></div><br>
                     <!--   <h4 class="title">Productos de la semana</h4> <br>-->
                        <?php
                        $query="SELECT nombre,id, imagen, precio_unidad FROM producto where estado='habilitado' and eliminar='no' "; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                            {
                            $id=$row['id'];
                             $nombre=$row['nombre'];
                            $imagen=$row['imagen'];
                            $precio_unidad=$row['precio_unidad'];
                                #echo "<a  href='informacion-producto.php?id=".$row['id']."'><i class='icon-bag'></i>".$row['nombre']."</a><button onclick='addproducto(".$row['id'].")'>+</button>";
                            ?>
                               
                            <div class="col-md-3">
                                <div class="product-box ">
                                    <div class="product border-theme br-0">
                                      <a  href="informacion-producto.php?id=<?php echo $id;?>"><img src="Administrador/assets/img/productos/<?php echo $imagen;?>" alt="<?php echo $nombre;?>" class="img-fluid ">
                                          <h4><?php echo $nombre;?></h4>
                                          <div class="product-right">
                                             <h4><?php echo $precio_unidad;?></h4>
                                         </div>
                                     </a>
                                        <button class="btn btn-theme theme-btn-sm" onclick="addproducto(<?php echo $id;?>)">Agregar al carrito <i class="icon-shopping-cart" ></i></button>
                                    </div>
                                </div>
                            </div>
                        <?php }  ?>
                   
                </div>
            </div>
            <!-- Title end -->
        </section>
        <!--productos de oferta fin -->
<hr/>
  

         <!-- Hot deal -->
   
    <!-- Hot deal End -->
    
       
        <!-- footer section 1 -->
        <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->
      <!-- Top Add to cart -->
<div id="cart_side" class="add_to_cart top">
    <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>my cart</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCart()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <ul class="cart_product">
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/electronics/product/5.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/fashion1/1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#"><img alt="" class="mr-2" src="assets/images/furniture/5.jpg"></a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4><span>1 x $ 299.00</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5>subtotal : <span>$299.00</span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="carrito.html" class="btn btn-theme theme-color theme-btn-sm">view cart</a>
                        <a href="checkout.html" class="btn btn-theme theme-color theme-btn-sm">checkout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Top Add to cart end-->
       

        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
         <script src="assets/js/popper.min.js" ></script> 
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
