<?php if(!isset($_SESSION)) 
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">
    <script>
        function ingrement_producto(id_carrito) {
          var cantidad=document.getElementById('addcantidad'+id_carrito).value;
          $.post('procesos/addcantidad.php', {
                      "id_carrito": id_carrito,
                      "cantidad":cantidad        
                    },function(data) {
                      //console.log('procesamiento finalizado', data);                           
                      if(data='error'){
                        window.location="carrito.php";
                      }
                      else{
                         //$("#seccioncarrito").load("#seccioncarrito"); 
                         window.location="carrito.php";
                      }
                  })
           console.log("reg ok");
        }
        function eliminar_producto(id_carrito) {
          
          $.post('procesos/eliminar_carrito.php', {
                      "id_carrito": id_carrito     
                    },function(data) {
                      //console.log('procesamiento finalizado', data);                           
                      if(data='error'){
                        window.location="carrito.php";
                      }
                      else{
                         //$("#seccioncarrito").load("#seccioncarrito"); 
                         window.location="carrito.php";
                      }
                  })
           //console.log("reg ok");
        }
    </script>
    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
         <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php' ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->

        <!-- Cart start-->
        <section class="cart-section" id="seccioncarrito">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-12">
        				<table class="table cart-table table-responsive-xs">
        					<thead>
        						<tr class="table-head">
        							<th scope="col">image</th>
        							<th scope="col">producto</th>
        							<th scope="col">tipo compra</th>
        							<th scope="col">cantidad</th>
        							<th scope="col">total</th>
        							<th scope="col"> </th>
        						</tr>
        					</thead>

        					<?php 
                                if (isset($_SESSION['user_id'])) 
                                { $subtotal_carr=0;
                                  $query_carr="SELECT carrito.id as id_carrito,producto.imagen,producto.nombre, producto.precio_unidad, producto.precio_cuarta, carrito.cantidad, carrito.tipo_compra FROM carrito join producto on carrito.id_producto=producto.id where carrito.id_usuario= $usuario_id "; 
                                     $resultado_carr=$conexion->query($query_carr); 
                                     while($row_carr=$resultado_carr->fetch_assoc())
                                        {
                                            $cantidad_cuarta_carr=$row_carr['cantidad']*3;
                                            if($row_carr['tipo_compra']=='unidad'){
                                                $total_product_carr=$row_carr['cantidad']*$row_carr['precio_unidad'];
                                                $subtotal_carr=$subtotal_carr+$total_product_carr;
                                                echo "
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <a href='#'>
                                                                <img class='mr-3' src='Administrador/assets/img/productos/".$row_carr['imagen']."' alt='Generic placeholder image'>
                                                            </a>
                                                        </td>
                                                        <td><a href='#'>".$row_carr['nombre']."</a>
                                                            <div class='mobile-cart-content row'>
                                                                <div class='col-xs-3'>
                                                                    <div class='qty-box'>
                                                                        <div class='input-group'>
                                                                            <input type='text' onchange='ingrement_producto(".$row_carr['id_carrito'].")' name='quantity' id='addcantidad".$row_carr['id_carrito']."' class='form-control input-number' value='".$row_carr['cantidad']."'>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class='col-xs-3'>
                                                                    <h2 class='td-color'>Bs. ". $row_carr['cantidad']*$row_carr['precio_unidad']."</h2>
                                                                </div>
                                                                <div class='col-xs-3'>
                                                                    <h2 class='td-color'>
                                                                        <a href='#' class='icon' onclick='eliminar_producto(".$row_carr['id_carrito'].")'>
                                                                            <i class='icon-close'></i>
                                                                        </a>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><h2>Unidades</h2></td>
                                                        <td >
                                                            <div class='qty-box'>
                                                                <div class='input-group'>
                                                                    <input type='text' onchange='ingrement_producto(".$row_carr['id_carrito'].")' name='quantity' id='addcantidad".$row_carr['id_carrito']."' class='form-control input-number' value='".$row_carr['cantidad']."'>
                                                                </div>
                                                            </div>
                                                        </td>                                                        
                                                        <td><h2>Bs. ".number_format((float)$row_carr['cantidad']*$row_carr['precio_unidad'], 2, '.', '')."</h2></td>
                                                        <td>
                                                            <a href='#' class='icon' onclick='eliminar_producto(".$row_carr['id_carrito'].")'>
                                                                <i class='icon-close'></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                ";
                                            }
                                            else{
                                                $total_producto_cuarta_carr=$row_carr['cantidad']*$row_carr['precio_cuarta'];
                                                $subtotal_carr=$subtotal_carr+$total_producto_cuarta_carr;
                                                echo "
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <a href='#'>
                                                                <img class='mr-3' src='Administrador/assets/img/productos/".$row_carr['imagen']."' alt='Generic placeholder image'>
                                                            </a>
                                                        </td>
                                                        <td><a href='#'>".$row_carr['nombre']."</a>
                                                            <div class='mobile-cart-content row'>
                                                                <div class='col-xs-3'>
                                                                    <div class='qty-box'>
                                                                        <div class='input-group'>
                                                                            <input type='text' onchange='ingrement_producto(".$row_carr['id_carrito'].")' name='quantity' id='addcantidad".$row_carr['id_carrito']."' class='form-control input-number' value='".$row_carr['cantidad']."'>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class='col-xs-3'>
                                                                    <h2 class='td-color'>Bs. ".number_format((float)$row_carr['cantidad']*$row_carr['precio_cuarta'], 2, '.', '')."</h2>
                                                                </div>
                                                                <div class='col-xs-3'>
                                                                    <h2 class='td-color'>
                                                                        <a href='#' class='icon' onclick='eliminar_producto(".$row_carr['id_carrito'].")'>
                                                                            <i class='icon-close'></i>
                                                                        </a>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><h2>Cuarta</h2></td>
                                                        <td >
                                                            <div class='qty-box'>
                                                                <div class='input-group'>
                                                                    <input type='text' onchange='ingrement_producto(".$row_carr['id_carrito'].")' name='quantity' id='addcantidad".$row_carr['id_carrito']."' class='form-control input-number' value='".$row_carr['cantidad']."'>
                                                                </div>
                                                            </div>
                                                        </td>                                                        
                                                        <td><h2>Bs. ".number_format((float)$row_carr['cantidad']*$row_carr['precio_cuarta'], 2, '.', '')."</h2></td>
                                                        <td>
                                                            <a href='#' class='icon' onclick='eliminar_producto(".$row_carr['id_carrito'].")'>
                                                                <i class='icon-close'></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                ";
                                            }
                                        }
                                }
                                 
                                ?> 
        				</table>
        				<table class="table cart-table table-responsive-md">
        					<tfoot>
        						<tr>
        							<td>total Bs.:</td>                                
        							<td><h2><?php 
                                    if (isset($_SESSION['user_id'])) 
                                   {
                                    echo number_format((float)$subtotal_carr, 2, '.', '');
                                    }
                                    else{echo "0.00";}
                                    ?> </h2></td>
        						</tr>
        					</tfoot>
        				</table>
        			</div>
        		</div>
        		<div class="row cart-buttons">
        			<div class="col-12 col-sm-6">
        				<a href="index.php" class="btn btn-theme theme-btn-sm">Seguir Comprando</a>
        			</div>
        			<div class="col-12 col-sm-6">
        				<a href="orden-de-compra.php" class="btn btn-theme theme-btn-sm">Terminar</a>
        			</div>
        		</div>
        	</div>
        </section>
        <!-- Cart End -->
        <!-- footer section 1 -->
       <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->
      <!-- Top Add to cart -->
<div id="cart_side" class="add_to_cart top">
    <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>my cart</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCart()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <ul class="cart_product">
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/electronics/product/5.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/fashion1/1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#"><img alt="" class="mr-2" src="assets/images/furniture/5.jpg"></a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4><span>1 x $ 299.00</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5>subtotal : <span>$299.00</span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="carrito.html" class="btn btn-theme theme-color theme-btn-sm">view cart</a>
                        <a href="checkout.html" class="btn btn-theme theme-color theme-btn-sm">checkout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Top Add to cart end-->
       

        <!-- cart start -->
        <div class="add_cart_btm_popup" id="fixed_cart_icon">
            <a href="#" class="fixed_cart">
                <i class="icon-shopping-cart"></i>
            </a>
        </div>
        <!-- cart end -->


        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
        <!-- <script src="assets/js/popper.min.js" ></script> -->
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
