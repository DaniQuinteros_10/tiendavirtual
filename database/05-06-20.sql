-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2020 a las 02:31:05
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_tienda_online`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id` int(250) NOT NULL,
  `id_producto` int(250) NOT NULL,
  `id_usuario` int(250) NOT NULL,
  `cantidad` int(10) NOT NULL,
  `tipo_compra` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id`, `id_producto`, `id_usuario`, `cantidad`, `tipo_compra`) VALUES
(12, 1, 14, 1, 'unidad'),
(13, 1, 14, 1, 'unidad'),
(14, 1, 14, 1, 'cuarta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(250) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Categoria 1', 'prueba  location.replace', 'habilitado'),
(2, 'Categoria 2', 'productos de higiene variados ', 'habilitado'),
(3, 'Categoria 3', 'prueba replace', 'habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_entrega`
--

CREATE TABLE `detalle_entrega` (
  `id` int(11) NOT NULL,
  `id_venta` int(250) NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `telefono` int(8) NOT NULL,
  `email` text NOT NULL,
  `latitud` varchar(250) NOT NULL,
  `longitud` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_entrega`
--

INSERT INTO `detalle_entrega` (`id`, `id_venta`, `nombre`, `apellido`, `telefono`, `email`, `latitud`, `longitud`) VALUES
(1, 10, 'Daniel', 'Quinteros', 4324, 'LLKHK', '12.3243232', '-32.3213123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id` int(250) NOT NULL,
  `id_venta` int(250) NOT NULL,
  `id_producto` int(250) NOT NULL,
  `cantidad` int(250) NOT NULL,
  `tipo_compra` text NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `estado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id`, `id_venta`, `id_producto`, `cantidad`, `tipo_compra`, `precio`, `estado`) VALUES
(1, 1, 1, 1, 'unidad', '10.00', ''),
(2, 1, 2, 3, 'cuarta', '20.00', ''),
(3, 2, 1, 2, 'unidad', '20.00', ''),
(4, 2, 3, 5, 'unidad', '50.00', ''),
(5, 3, 4, 10, 'unidad', '50.00', ''),
(6, 3, 5, 9, 'cuarta', '63.00', ''),
(7, 6, 1, 3, 'cuarta', '30.00', ''),
(8, 7, 2, 9, 'cuarta', '60.00', ''),
(9, 8, 6, 1, 'unidad', '10.00', ''),
(10, 4, 1, 1, 'unidad', '10.00', ''),
(11, 5, 2, 2, 'unidad', '31.20', ''),
(12, 10, 1, 1, 'unidad', '10.00', ''),
(13, 10, 2, 0, 'cuarta', '0.00', ''),
(14, 10, 3, 2, 'unidad', '70.00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_producto`
--

CREATE TABLE `galeria_producto` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `img_gal` text NOT NULL,
  `seo` varchar(150) NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria_producto`
--

INSERT INTO `galeria_producto` (`id`, `id_producto`, `img_gal`, `seo`, `estado`) VALUES
(1, 2, '6797ec396e4f3c11a4cf08ad520d4bad.jpg', 'nubes', 'habilitado'),
(5, 4, 'Camera.png', 'icono camara', 'habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id` int(250) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Marca 1', 'lava vajillas, detergentes en general', 'habilitado'),
(2, 'Marca 2', 'empresa de venta de productos de servicio de cuidado de la piel', 'habilitado'),
(3, 'Marca 3', 'camarones en todo y con todo lo que te puedas imaginar', 'habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(250) NOT NULL,
  `id_subcategoria` int(250) NOT NULL,
  `id_marca` int(250) NOT NULL,
  `nombre` text NOT NULL,
  `presentacion` text NOT NULL,
  `precio_unidad` decimal(8,2) NOT NULL,
  `precio_cuarta` decimal(8,2) NOT NULL,
  `precio_adquisicion` decimal(8,2) NOT NULL,
  `cantidad` int(250) NOT NULL,
  `imagen` text NOT NULL,
  `estado` varchar(20) NOT NULL,
  `eliminar` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `id_subcategoria`, `id_marca`, `nombre`, `presentacion`, `precio_unidad`, `precio_cuarta`, `precio_adquisicion`, `cantidad`, `imagen`, `estado`, `eliminar`) VALUES
(1, 1, 1, 'producto 1', '100 gr', '10.00', '30.00', '9.00', 57, 'Paint Palette.png', 'habilitado', 'no'),
(2, 1, 1, 'producto 2', '300 gr', '15.60', '20.00', '10.00', 51, 'Paint Palette.png', 'deshabilitado', 'no'),
(3, 1, 3, 'producto 3', '2 kl', '35.00', '50.00', '30.00', 50, 'Paint Palette.png', 'habilitado', 'no'),
(4, 1, 3, 'producto 4', '500 ml', '5.00', '10.00', '34.00', 50, 'Coffee Cup.png', 'habilitado', 'no'),
(5, 1, 2, 'producto 5', '500 ml', '12.00', '21.00', '10.00', 8, 'images.jpg', 'habilitado', 'no'),
(6, 1, 3, 'producto 6', '500 ml', '10.00', '30.00', '9.00', 50, 'Paint Palette.png', 'habilitado', 'no'),
(7, 1, 2, 'producto 7', '500 ml', '12.00', '30.00', '9.00', 23, '', 'habilitado', 'no'),
(8, 1, 2, 'producto 8', '50 ml', '31.00', '21.00', '12.00', 3, '', 'habilitado', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre_rol` varchar(50) NOT NULL,
  `descripcion` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre_rol`, `descripcion`) VALUES
(1, 'administrar', 'rol que tiene acceso a todas las pestaÃ±as'),
(2, 'distribuidor', 'el usuario que se encarga de transportar los pedidos hasta el cliente'),
(3, 'almacen', 'usuario encargado de proveer los productos para pedidos'),
(4, 'cliente', 'rol para los compradores o clientes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estado_sc` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `id_categoria`, `nombre`, `descripcion`, `estado_sc`) VALUES
(1, 2, 'sub1', 'desc1', 'habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(250) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` text NOT NULL,
  `id_rol` int(11) NOT NULL,
  `nombre_usuario` text NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(20) NOT NULL,
  `eliminar` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `email`, `id_rol`, `nombre_usuario`, `password`, `estado`, `eliminar`) VALUES
(3, 'Rocio', 'caballero', 'admin@gmail.com', 3, 'admin@gmail.com', '5NbkxdvQ3qalqA==', 'habilitado', 'no'),
(6, 'susi', 'oria del campo', 'susi@gmail.com', 1, 'admin', '5NbkxdvQ3qalqA==', 'habilitado', 'no'),
(8, 'Valeria Estefania', 'Rodriguez', 'valeriamolina.coach@gmail.com', 3, 'valeriaestefania', '5NbkxdvQ3qalqA==', 'habilitado', 'no'),
(9, 'Paola', 'Garcia', '', 1, 'user', '5NbkxdvQ3qalqA==', 'habilitado', 'no'),
(14, 'Daniel', 'Quinteros', '', 3, 'dani123', '5NbkxdvQ3qalqA==', 'habilitado', 'no'),
(15, 'Ana', 'Carmona', '', 2, 'ana', '5NbkxdvQ3qalqA==', 'habilitado', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(250) NOT NULL,
  `id_usuario` int(250) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_usuario`, `fecha`, `estado`) VALUES
(1, 3, '2020-05-22 18:00:00', ''),
(2, 3, '2020-05-22 18:00:00', ''),
(3, 3, '2020-05-22 18:00:00', ''),
(4, 3, '2020-05-22 18:00:00', ''),
(5, 3, '2020-05-22 18:00:00', ''),
(6, 9, '2020-05-22 18:00:00', ''),
(7, 9, '2020-05-22 18:00:00', ''),
(8, 9, '2020-05-22 18:00:00', ''),
(9, 14, '2020-06-02 21:55:18', ''),
(10, 14, '2020-06-02 22:02:45', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_entrega`
--
ALTER TABLE `detalle_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `id_venta` (`id_venta`);

--
-- Indices de la tabla `galeria_producto`
--
ALTER TABLE `galeria_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_subcategoria`),
  ADD KEY `id_marca` (`id_marca`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `detalle_entrega`
--
ALTER TABLE `detalle_entrega`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `galeria_producto`
--
ALTER TABLE `galeria_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `detalle_venta_ibfk_2` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id`),
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`id_marca`) REFERENCES `marca` (`id`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
