
<div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <div class="theme-menu xs-space-tb">
                                <div class="brand-logo">
                                    <a href="index.php"> <img src="assets/images/logo13.png" class="img-fluid" alt=""></a>
                                </div>
                                <div class="theme-searcbar">
                                    <form class="theme-form-two d-none d-xl-block right-space">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputPassword2" placeholder="Buscar artículos" required="required">
                                            <button type="submit" class="btn btn-theme in-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </form>
                                    <div class="icon-nav-cart dark-theme-icon box-icon" id="listacarrito">
                                        <ul class="header-three">
                                            <li class="fix-top searchbox">
                                                <div class="searchbox-md d-xl-none" title="Quick Search">
                                                    <i class="flaticon flaticon-search" onclick="openSearch()"></i>
                                                </div>
                                            </li>
                                            <li class="fix-top toggle-nav">
                                                <div>
                                                    <i class="flaticon flaticon-menu"></i>
                                                </div>
                                            </li>
                                            
                                            
                                            <li class="fix-top onhover-div mobile-cart">
                                                <div title="Show Cart" class="cart-icon">
                                                    <i class="flaticon flaticon-shopping-bag"></i>
                                                    <?php
                                                      if (isset($_SESSION['user_id'])) 
                                                        {
                                                          $query="SELECT count(id) as contador FROM carrito where id_usuario= $usuario_id  "; 
                                                             $resultado=$conexion->query($query); 
                                                             while($row=$resultado->fetch_assoc())
                                                                {
                                                                    echo "<span class='cart-add'>".$row['contador']."</span>";
                                                                }
                                                        }
                                                      else{echo "<span class='cart-add'>0</span>";}  
                                                    ?>
                                                    
                                                </div>
                                                <ul class="show-div shopping-cart">
                                                    <?php 
                                                        if (isset($_SESSION['user_id'])) 
                                                        { $subtotal=0;
                                                          $query1="SELECT producto.imagen,producto.nombre, producto.precio_unidad, producto.precio_cuarta, carrito.cantidad, carrito.tipo_compra FROM carrito join producto on carrito.id_producto=producto.id where carrito.id_usuario= $usuario_id "; 
                                                             $resultado1=$conexion->query($query1); 
                                                             while($row1=$resultado1->fetch_assoc())
                                                                {
                                                                    $cantidad_cuarta=$row1['cantidad']*3;
                                                                    if($row1['tipo_compra']=='unidad'){
                                                                        $total_producto=$row1['cantidad']*$row1['precio_unidad'];
                                                                        $subtotal=$subtotal+$total_producto;
                                                                        echo "
                                                                        <li>
                                                                            <div class='media'>
                                                                                <a href='#'><img class='mr-3' src='Administrador/assets/img/productos/".$row1['imagen']."' alt='Generic placeholder image'></a>
                                                                                <div class='media-body'>
                                                                                    <a href='#'>
                                                                                        <h4>".$row1['nombre']."</h4>
                                                                                    </a>
                                                                                    <h4><span>Und|".$row1['cantidad']." x Bs. ". $row1['cantidad']*$row1['precio_unidad']."</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class='close-circle'>
                                                                                <a href='#'><i class='fa fa-times' aria-hidden='true'></i></a>
                                                                            </div>
                                                                        </li>
                                                                        ";
                                                                    }
                                                                    else{
                                                                        $total_producto_cuarta=$row1['cantidad']*$row1['precio_cuarta'];
                                                                        $subtotal=$subtotal+$total_producto_cuarta;
                                                                        echo "
                                                                        <li>
                                                                            <div class='media'>
                                                                                <a href='#'><img class='mr-3' src='Administrador/assets/img/productos/".$row1['imagen']."' alt='Generic placeholder image'></a>
                                                                                <div class='media-body'>
                                                                                    <a href='#'>
                                                                                        <h4>".$row1['nombre']."</h4>
                                                                                    </a>
                                                                                    <h4><span>CU|".$row1['cantidad']." x Bs. ". $row1['cantidad']*$row1['precio_cuarta']."</span></h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class='close-circle'>
                                                                                <a href='#'><i class='fa fa-times' aria-hidden='true'></i></a>
                                                                            </div>
                                                                        </li>
                                                                        ";
                                                                    }
                                                                }
                                                        }
                                                         
                                                        ?>  
                                                    
                                                    <li>
                                                        <div class="total">
                                                            <h5>subtotal : <span>Bs. <?php 
                                                                if (isset($_SESSION['user_id'])) 
                                                                   {echo number_format((float)$subtotal, 2, '.', '');}
                                                               else{ echo"0.00";}
                                                            
                                                            ?></span></h5>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="buttons">
                                                            <a href="carrito.php" class="view-cart">Ver carrito</a>
                                                            <a href="orden-de-compra.php" class="checkout">Terminar</a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <span class="cart-price ml-0">Bs. <?php 
                                                                if (isset($_SESSION['user_id'])) 
                                                                   {echo number_format((float)$subtotal, 2, '.', '');}
                                                               else{ echo"0.00";}
                                                            
                                                            ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>