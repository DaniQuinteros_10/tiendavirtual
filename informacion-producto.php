<?php if(!isset($_SESSION)) 
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>
<?php include "Administrador/php/conexion.php";?>
<?php
$query="SELECT nombre,id, imagen, precio_unidad, precio_cuarta FROM producto where estado='habilitado' and eliminar='no' and id=$_GET[id]"; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
{
    $id=$row['id'];
    $nombre=$row['nombre'];
    $imagen=$row['imagen'];
    $precio_unidad=$row['precio_unidad']; 
    $precio_cuarta=$row['precio_cuarta']; 
    ?>
<?php } ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">

      <script type="text/javascript">
        function addcarrito(id){
            var tipo_compra;
            if(document.getElementById('tipo_compra1').checked){
                tipo_compra=document.getElementById('tipo_compra1').value;
            } 
            else{
                tipo_compra=document.getElementById('tipo_compra2').value;
            }
            var cantidad =document.getElementById('cantidad').value;
            
            $.post('procesos/addcarrito.php', {
                      "tipo_compra": tipo_compra,
                      "cantidad":cantidad,
                      "id_producto":id              
                    },function(data) {
                      //console.log('procesamiento finalizado', data);                           
                      if(data='error'){
                        window.location="login.php";
                      }
                      else{
                         //$("#listacarrito").load("#listacarrito"); 
                         $("#listacarrito").load();
                         window.location="carrito.php";
                      }
                  })
           console.log("reg ok");  
        }
      </script>  
    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
        <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php' ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->

<!-- Producr 3 column page -->
<section>
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 collection-filter">
                    <!--<div class="collection-filter-block">
                        <div class="collection-mobile-back">
                                <span class="filter-back">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i> back
                                </span>
                        </div>
                        <div class="collection-collapse-block border-0 open">
                            <h3 class="collapse-block-title">brand</h3>
                            <div class="collection-collapse-block-content">
                                <div class="collection-brand-filter">
                                    <ul class="category-list">
                                        <li><a href="#">clothing</a></li>
                                        <li><a href="#">bags</a></li>
                                        <li><a href="#">footwear</a></li>
                                        <li><a href="#">watches</a></li>
                                        <li><a href="#">accessories</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <!-- side-bar single product slider start -->
                    <div class="theme-card">
                        <h5 class="title-border">productos similares</h5>
                        <div class="offer-slider slide-1">
                            <div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-1.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                        
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-1.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                        
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-3.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                        
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-4.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                       
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-5.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                        
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a href="" ><img class="img-fluid " src="assets/images/product/p-3-6.jpg" alt=""></a>
                                    <div class="media-body align-self-center">
                                        
                                        <a href="product-page(no-sidebar).html"><h6>Slim Fit Cotton Shirt</h6></a>
                                        <h4>$500.00</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- side-bar single product slider end 
                    <div class="service-two border-gray border">
                        <div class="service-classic">
                            <i class="flaticon-truck"></i>
                            <h4>Free Shipping</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-telemarketer"></i>
                            <h4>Online Service</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-24-hours"></i>
                            <h4>24 x 7 Service</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-credit-card"></i>
                            <h4>Online Payment</h4>
                        </div>
                    </div>-->
                    <!-- side-bar banner start here -->
                    <div class="collection-sidebar-banner">
                        <a href="#">
                            <img src="assets/images/sidebar_small_banner.jpg" class="img-fluid " alt="">
                        </a>
                    </div>
                    <!-- side-bar banner end here -->
                </div>
                <div class="col-lg-9 col-sm-12 col-xs-12">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="filter-main-btn mb-3">
                                    <span class="filter-btn"><i class="fa fa-bars" aria-hidden="true"></i> sidebar</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="product-slick">
                                    <!-- primera imagen -->
                                        <div>
                                            <img src="Administrador/assets/img/productos/<?php echo $imagen;?>" alt="<?php echo $nombre;?>" class="img-fluid image_zoom_cls-0 ">
                                        </div>
                                   <!-- galeria productos -->
                                    
                                    <?php
                                        $query="SELECT img_gal,seo, estado FROM galeria_producto where estado='habilitado' and id_producto=$_GET[id]"; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                                        {
                                          
                                            $img_gal=$row['img_gal'];
                                            $seo=$row['seo'];
                                            ?>
                                            <div>
                                                <img src="Administrador/assets/img/productos/galeria/<?php echo $img_gal;?>" alt="<?php echo $seo;?>" class="img-fluid image_zoom_cls-3 ">
                                            </div>
                                        <?php } ?>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="slider-nav">
                                            <!-- primera imagen de producto -->
                                            <div>
                                                <img src="Administrador/assets/img/productos/<?php echo $imagen;?>" alt="<?php echo $nombre;?>" class="img-fluid image_zoom_cls-0 ">
                                            </div>
                                            <!-- galeria imagenes producto -->
                                            <?php
                                        $query="SELECT img_gal,seo, estado FROM galeria_producto where estado='habilitado' and id_producto=$_GET[id]"; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                                        {
                                          
                                            $img_gal=$row['img_gal'];
                                            $seo=$row['seo'];
                                            ?>
                                            <div>
                                                <img src="Administrador/assets/img/productos/galeria/<?php echo $img_gal;?>" alt="<?php echo $seo;?>" class="img-fluid image_zoom_cls-3 ">
                                            </div>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="product-right product-form-box">
                                 <h2> <?php echo $nombre;?> </h2>
                                 <p>Precio unidad </p>
                                 <h4><?php echo $precio_unidad;?> Bs</h4>
                                <span> Precio por cuarta </span>
                                <h3><?php echo $precio_unidad;?> Bs</h3>
                                    <hr/>
                                    <h6 class="product-title ">Cantidad</h6>
                                    <!-- por unidad -->
                                    <div class="product-description ">
                                        <!-- por unidad -->
                                        <div class="custom-control custom-checkbox collection-filter-checkbox">
                                            <div class="row">
                                               <div class="col-lg-6">
                                                 <input type="radio" name="tipo_compra" id="tipo_compra1" value="unidad"> Unidad
                                             </div>
                                             <div class="col-lg-6">
                                                <input type="radio" name="tipo_compra" id="tipo_compra2" value="cuarta"> Cuarta<br>
                                            </div>
                                        </div>
                                           
                                        </div>
                                        <div class="qty-box">
                                            <div class="input-group">
                                              <span class="input-group-prepend">
                                                <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">
                                                 <i class="icon-angle-left"></i>
                                             </button>
                                         </span>
                                         <input type="text"  name="quantity" id="cantidad" class="form-control input-number" value="1">
                                         <span class="input-group-prepend">
                                            <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                                <i class="icon-angle-right"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                    </div>
                                    <div class="product-buttons">
                                        
                                        <button type="button" class="btn btn-theme btn-block" onclick="addcarrito(<?php echo $_GET['id'];?>)">
                                                Agregar al Carrito <i class="icon-shopping-cart" ></i>
                                        </button>
                                        <!--<a href="orden-de-compra.php" class="btn btn-theme theme-btn-sm">buy now</a>-->
                                    </div>
                                    <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- Product descrption 
                        <section class="tab-product m-0 p-t-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 col-lg-12">
                                        <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="top-home-tab" data-toggle="tab" href="#top-home" role="tab" aria-selected="true">Description</a>
                                                <div class="material-border"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="profile-top-tab" data-toggle="tab" href="#top-profile" role="tab" aria-selected="false">Details</a>
                                                <div class="material-border"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="contact-top-tab" data-toggle="tab" href="#top-contact" role="tab" aria-selected="false">Video</a>
                                                <div class="material-border"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="review-top-tab" data-toggle="tab" href="#top-review" role="tab" aria-selected="false">Write Review</a>
                                                <div class="material-border"></div>
                                            </li>
                                        </ul>
                                        <div class="tab-content nav-material" id="top-tabContent">
                                            <div class="tab-pane fade show active" id="top-home" role="tabpanel" aria-labelledby="top-home-tab">
                                                <p>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                </p>
                                            </div>
                                            <div class="tab-pane fade" id="top-profile" role="tabpanel" aria-labelledby="profile-top-tab">
                                                <p>
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                                </p>
                                                <div class="single-product-tables">
                                                    <table>
                                                        <tbody><tr>
                                                            <td>Febric</td>
                                                            <td>Chiffon</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Color</td>
                                                            <td>Red</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Material</td>
                                                            <td>Crepe printed</td>
                                                        </tr>
                                                        </tbody></table>
                                                    <table>
                                                        <tbody><tr>
                                                            <td>Length</td>
                                                            <td>50 Inches</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Size</td>
                                                            <td>S, M, L .XXL</td>
                                                        </tr>
                                                        </tbody></table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
                                                <div class="t-space-30 text-center">
                                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/BUWzX78Ye_8"  allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="top-review" role="tabpanel" aria-labelledby="review-top-tab">

                                                <form class="theme-form-one">
                                                    <div class="form-row">
                                                        
                                                        <div class="col-md-6">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control w-100" id="name" placeholder="Enter Your name" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="email">Email</label>
                                                            <input type="text" class="form-control w-100" id="email" placeholder="Email" required>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="review">Review Title</label>
                                                            <input type="text" class="form-control w-100" id="review" placeholder="Enter your Review Subjects" required>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="review">Review Title</label>
                                                            <textarea class="form-control w-100" placeholder="Wrire Your Testimonial Here" id="exampleFormControlTextarea1" rows="6"></textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <button class="btn btn-theme theme-btn-sm" type="submit">Submit Your Review</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>-->
                        <!-- product descrption end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Producr 3 col left page end -->

       
        <!-- footer section 1 -->
        <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->

       

        <!-- cart start -->
        <div class="add_cart_btm_popup" id="fixed_cart_icon">
            <a href="#" class="fixed_cart">
                <i class="icon-shopping-cart"></i>
            </a>
        </div>
        <!-- cart end -->


        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
         <script src="assets/js/popper.min.js" ></script>
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
