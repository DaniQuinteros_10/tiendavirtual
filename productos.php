<?php if(!isset($_SESSION)) 
    { 
        include "Administrador/php/control-sesion.php";
    } 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Tienda en linea ">
        <meta name="keywords" content="tienda en linea, compras por internet">
        <meta name="author" content="Rocio">
        <link rel="icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="../assets/images/favicon/favicon13.png" type="image/x-icon"/>
        <title>Demo 1</title>

        <!--Google font-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

        <!-- Slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">

        <!-- Flaticon icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

        <!-- Themify icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/themify.css">

        <!-- Animate icon -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

        <!-- Color css -->
        <link rel="stylesheet" type="text/css" href="assets/css/style4.css" id="color">

    </head>
    <body>

<!-- loader start -->
 <div class="loader-wrapper">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- loader end -->

        <!-- Header -->
        <header class="sm-top-space">
           
            <!-- add banner end-->
            <div class="header-three mobile-fix-option"></div>
            <?php include 'top-bar.php' ?>
            <div class="theme-header theme-bg main-header">
                <?php include 'menu-superior.php' ?>
                <?php include 'menu-inferior.php' ?>
            </div>
        </header>
        <!-- Header End-->

<!-- category page -->
<section class="section-b-space">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">

                <div class="col-sm-3 collection-filter">
                    <!-- side-bar colleps block stat -->
                    <div class="collection-filter-block">
                        <!-- brand filter start -->
                        <div class="collection-mobile-back">
                        	<span class="filter-back">
                        		<i class="fa fa-angle-left" aria-hidden="true"></i> back
                        	</span>
                        </div>
                        <div class="collection-collapse-block open">
                            <h3 class="collapse-block-title">Categoria 1</h3>
                            <div class="collection-collapse-block-content">
                                <div class="collection-brand-filter">
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="zara">
                                        <label class="custom-control-label" for="zara">zara</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="vera-moda">
                                        <label class="custom-control-label" for="vera-moda">vera moda</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="forever-21">
                                        <label class="custom-control-label" for="forever-21">forever 21</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="roadster">
                                        <label class="custom-control-label" for="roadster">roadster</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="only">
                                        <label class="custom-control-label" for="only">only</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <!-- price filter start here -->
                        <div class="collection-collapse-block border-0 open">
                            <h3 class="collapse-block-title">Precio</h3>
                            <div class="collection-collapse-block-content">
                                <div class="collection-brand-filter">
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="hundred">
                                        <label class="custom-control-label" for="hundred">$10 - $100</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="twohundred">
                                        <label class="custom-control-label" for="twohundred">$100 - $200</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="threehundred">
                                        <label class="custom-control-label" for="threehundred">$200 - $300</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="fourhundred">
                                        <label class="custom-control-label" for="fourhundred">$300 - $400</label>
                                    </div>
                                    <div class="custom-control custom-checkbox collection-filter-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="fourhundredabove">
                                        <label class="custom-control-label" for="fourhundredabove">$400 above</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- silde-bar colleps block end here -->
                   
                    <!-- side-bar single product slider end -->
                    <div class="service-two border-gray border">
                        <div class="service-classic">
                            <i class="flaticon-truck"></i>
                            <h4>Free Shipping</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-telemarketer"></i>
                            <h4>Online Service</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-24-hours"></i>
                            <h4>24 x 7 Service</h4>
                        </div>
                        <div class="service-classic">
                            <i class="flaticon-credit-card"></i>
                            <h4>Online Payment</h4>
                        </div>
                    </div>
                    <!-- side-bar banner start here -->
                    <div class="collection-sidebar-banner">
                        <a href="#">
                            <img src="assets/images/sidebar_small_banner.jpg" class="img-fluid " alt="">
                        </a>
                    </div>
                    <!-- side-bar banner end here -->
                </div>
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="container-fluid p-0">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="top-banner-wrapper">
                                        <a href="#"><img src="assets/images/mega-menu-banner-1.jpg" class="img-fluid " alt=""/></a>
                                        <div class="top-banner-content small-section">
                                            <h5 >Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                                        </div>
                                    </div>
                                    <div class="collection-product-wrapper">
                                        <div class="product-top-filter">
                                            <div class="container-fluid p-0">
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="filter-main-btn">
                                                            <span class="filter-btn btn btn-theme theme-btn-sm"><i class="fa fa-filter" aria-hidden="true"></i> Filter</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="product-filter-content">
                                                            <div class="search-count">
                                                                <h5>Showing Products 1-24 of 10 Result</h5>
                                                            </div>
                                                            <div class="collection-view">
                                                                <ul>
                                                                    <li><i class="fa fa-th grid-layout-view"></i></li>
                                                                    <li><i class="fa fa-list-ul list-layout-view"></i></li>
                                                                </ul>
                                                            </div>
                                                            <div class="collection-grid-view">
                                                                <ul>
                                                                    <li>
                                                                        <img src="assets/images/icon/2.png" class="product-2-layout-view" alt="collection">
                                                                    </li>
                                                                    <li>
                                                                        <img src="assets/images/icon/3.png" class="product-3-layout-view" alt="collection">
                                                                    </li>
                                                                    <li>
                                                                        <img src="assets/images/icon/4.png" class="product-4-layout-view" alt="collection">
                                                                    </li>
                                                                    <li>
                                                                        <img src="assets/images/icon/6.png" class="product-6-layout-view" alt="collection">
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="product-page-per-view">
                                                                <select>
                                                                    <option value="High to low">24 Products Par Page</option>
                                                                    <option value="Low to High">50 Products Par Page</option>
                                                                    <option value="Low to High">100 Products Par Page</option>
                                                                </select>
                                                            </div>
                                                            <div class="product-page-filter">
                                                                <select>
                                                                    <option value="High to low">Sorting items</option>
                                                                    <option value="Low to High">50 Products</option>
                                                                    <option value="Low to High">100 Products</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="product-wrapper-grid">
                                            <div class="container-fluid">
                                                <div class="row sapce-category">
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">
                                                                <img src="assets/images/product/pro_5.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                     <button title="Add to cart" onclick="openCart()" data-toggle="modal" class="cart-modal" data-target="add_to_cart"><i class="icon-bag"></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                   
                                                                    <a href="informacion-producto.html"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">
                                                                <img src="assets/images/product/pro_6.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                    <button><i class="icon-bag" ></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                   
                                                                    <a href="#"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">
                                                                <div class="lable-wrapper">
                                                                    <span class="lable1">new</span>
                                                                    <span class="lable2">10% off</span>
                                                                </div>
                                                                <img src="assets/images/product/p-3-1.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                    <button><i class="icon-bag" ></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                   
                                                                    <a href="#"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">
                                                                <div class="lable-wrapper">
                                                                    <span class="lable1">new</span>
                                                                    <span class="lable2">10% off</span>
                                                                </div>
                                                                <img src="assets/images/product/p-3-2.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                    <button><i class="icon-bag" ></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                    
                                                                    <a href="#"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">

                                                                <img src="assets/images/product/p-3-1.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                    <button><i class="icon-bag" ></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                    
                                                                    <a href="#"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="product border-theme">

                                                                <img src="assets/images/product/p-3-2.jpg" alt="product" class="img-fluid ">
                                                                <div class="cart-info">
                                                                    <button><i class="icon-bag" ></i></button>
                                                                    <a href="#" data-toggle="modal" data-target="#quick-view"><i class="icon-search" aria-hidden="true"></i></a>
                                                                    <a href="#"><i class="icon-heart" aria-hidden="true"></i></a>
                                                                    <a href="compare.html"><i class="icon-arrows-horizontal" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <div>
                                                                   
                                                                    <a href="#"><h6>When an unknown.</h6></a>
                                                                    <h4>$45.00</h4>
                                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-pagination">
                                            <div class="theme-paggination-block">
                                                <div class="container-fluid p-0">
                                                    <div class="row">
                                                        <div class="col-xl-6 col-md-6 col-sm-12">
                                                            <nav aria-label="Page navigation">
                                                                <ul class="pagination">
                                                                    <li class="page-item">
                                                                        <a class="page-link" href="#" aria-label="Previous">
                                                                            <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                                    <li class="page-item">
                                                                        <a class="page-link" href="#" aria-label="Next">
                                                                            <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </nav>
                                                        </div>
                                                        <div class="col-xl-6 col-md-6 col-sm-12">
                                                            <div class="product-search-count-bottom">
                                                                <h5>Showing Products 1-24 of 10 Result</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- category page end -->

        <!-- footer section 1 -->
        <?php include 'footer.php' ?>
        <!-- footer section end -->

        

        <!-- Tap on Top -->
        <div class="tap-top">
           <div>
              <i class="fa fa-angle-double-up"></i>
           </div>
        </div>
        <!-- Tap on Ends -->

       

      <!-- Top Add to cart -->
<div id="cart_side" class="add_to_cart top">
    <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>my cart</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCart()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media">
            <ul class="cart_product">
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/electronics/product/5.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#">
                            <img alt="" class="mr-2" src="assets/images/fashion1/1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4>
                                <span>1 x $ 299.00</span>
                            </h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="media">
                        <a href="#"><img alt="" class="mr-2" src="assets/images/furniture/5.jpg"></a>
                        <div class="media-body">
                            <a href="#">
                                <h4>item name</h4>
                            </a>
                            
                            <h4><span>1 x $ 299.00</span></h4>
                        </div>
                    </div>
                    <div class="close-circle">
                        <a href="#">
                            <i class="ti-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="cart_total">
                <li>
                    <div class="total">
                        <h5>subtotal : <span>$299.00</span></h5>
                    </div>
                </li>
                <li>
                    <div class="buttons">
                        <a href="carrito.html" class="btn btn-theme theme-color theme-btn-sm">view cart</a>
                        <a href="checkout.html" class="btn btn-theme theme-color theme-btn-sm">checkout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Top Add to cart end-->


        

        <!-- latest jquery-->
        <script src="assets/js/jquery-3.3.1.min.js" ></script>
        <script src="assets/js/jquery-ui.min.js" ></script>
        <!-- Exit js -->
        <script src="assets/js/exit.js" ></script>
        <script  src="assets/js/jquery.exitintent.js"></script>
        <!-- popper js-->
        <script src="assets/js/popper.min.js" ></script>
        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.js" ></script>
        <!-- slick js-->
        <script src="assets/js/slick.js" ></script>
        <!-- Menu js -->
        <script src="assets/js/menu.js"></script>
        <!-- bootstrap-notify js -->
        <script src="assets/js/bootstrap-notify.min.js"></script>
        <!--fly cart-->
        <script src="assets/js/fly-cart.js" ></script>
        <!-- Custome scripts js-->
        <script src="assets/js/custom-scripts.js" ></script>
        <!-- <script src="assets/js/timer-two.js" ></script> -->


    </body>
</html>
