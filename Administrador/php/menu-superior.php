<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <!--<li class="nav-item d-none d-sm-inline-block">-->
      <!--  <a href="" class="nav-link">Ver sitio</a>-->
      <!--</li>-->
      <!--<li class="nav-item d-none d-sm-inline-block">-->
      <!--  <a href="#" class="nav-link">Contact</a>-->
      <!--</li>-->
    </ul>

    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
         <h3 class="profile-username text-center"><i class="fas fa-user mr-2"></i><?php echo $usuario_nombre;?></h3>
            
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Opciones</span>
          <div class="dropdown-divider"></div>
          <a href="modificar-usuario.php?user=<?php echo $usuario_id;?>" class="dropdown-item">
            <i class="fas fa-pencil-alt mr-2"></i>Ver perfil
           
          </a>
          <div class="dropdown-divider"></div>
          <a href="../php/logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i> Cerrar sesión
           
          </a>
          
        </div>
      </li>
    </ul>
</nav>