
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" >
     
      <h3>TEAM M&L</h3>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) 
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>-->

      <!-- Sidebar Menu -->
      <?php
        //MENU PARA EL DISTRIBUIDOR
        if($usuario_id_rol== "2"){
      ?>
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
           <li class="nav-header">Ventas</li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Pedidos
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-pedidos.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista de pedidos</p>
                  </a>
                </li>                
              </ul>
            </li>
          </ul>
        </nav>

      <?php    
        }
      //MENU PARA EL  ADMIN  
       else { 
      ?>
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
           <li class="nav-header">USUARIOS</li>

              <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Roles
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-roles.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista roles</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="registrar-rol.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrar nuevo</p>
                  </a>
                </li>
              </ul>
              </li>
              
              <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Usuarios
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-usuarios.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista usuarios</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="registrar-usuario.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrar nuevo</p>
                  </a>
                </li>
              </ul>
              </li>
              <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Clientes
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-clientes.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista clientes</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="registrar-usuario.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrar nuevo</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-header">PRODUCTOS</li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Categoria
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="lista-categoria.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lista categoria</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="registrar-categoria.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Registrar nuevo</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Sub categoria
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="lista-subcategoria.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lista sub categoria</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="registrar-subcategoria.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Registrar nuevo</p>
                    </a>
                  </li>
                </ul>
              </li>
             <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Marca
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
               <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-marcas.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista marcas</p>
                  </a>
                </li>
                
                
                <li class="nav-item">
                  <a href="registrar-marca.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrar nuevo</p>
                  </a>
                  
                </li>
               </ul>
             </li>
              <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                  Productos
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
               <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="lista-productos.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Lista productos</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="registrar-producto.php" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registrar nuevo</p>
                  </a>
                  
                </li>
               </ul>
             </li>
            
            <li class="nav-header">STOCK PRODUCTOS</li>

              <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-circle"></i>
                    <p>
                    Stock
                    <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="lista-stock.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lista stock</p>
                      </a>
                    </li>
                  </ul>
              </li>

            <li class="nav-header">REPORTES</li>
                <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Clientes
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="lista-clientes-reporte.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lista clientes</p>
                    </a>
                  </li>
                </ul>
                <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Productos
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="lista-reporte-productos.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Lista productos</p>
                    </a>
                  </li>
                </ul>
            </li>
               
          </ul>
        </nav>
      <?php
        }
      ?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>