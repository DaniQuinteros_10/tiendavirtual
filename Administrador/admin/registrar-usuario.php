<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; ?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons 
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  
  <script src="../assets/js/jquery-2.2.4.min.js"></script>
  <!-- funciones -->
  <script src="funciones/modulo-usuarios.js"></script>
  
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar usuario</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div role="form">
                <div class="card-body">
                  <div class="row">
                    
                    
                   
                    <div class="col-sm-6 ">
                        <div class="form-group">
                          <label >Nombres</label>
                          <input type="text" class="form-control" id="nombre">
                        </div>
                        <div class="form-group">
                          <label >Apellidos</label>
                          <input type="text" class="form-control" id="apellido">
                        </div>
                        <div class="form-group">
                          <label >Correo electrónico</label>
                          <input type="email" class="form-control"  id="email">
                        </div>
                          
                                            
                    </div>
                    <div class="col-sm-6 ">

                        <div class="form-group">
                          <label >Nombre usuario</label>
                          <input type="text" class="form-control"  id="nombre_usuario">

                        </div>
                        <div class="form-group">
                          <label >Contraseña</label>
                          <input type="password" class="form-control"  id="password">
                        </div>
                        <div class="form-group">
                          <label >Tipo usuario</label>
                          <select class="form-control" id="tipo_usuario" disabled>
                            <option value="1"> administrador</option>
                           
                          </select>
                        </div>
                      </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="buttton" class="btn btn-success" onclick="guardar_usuario()" >Guardar</button>
                   <button type="buttton" class="btn btn-primary" onclick="cancelar_funcion()">Cancelar</button>
                </div>
              </div>
              <div id="registro"></div>
            </div>
            <!-- /.card -->
          </div>
        </div>
     </div>
    </section>
    <!-- /.content -->
    

    
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>


</body>
</html>
