<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; 
  //categorias
   $sql1 = "select  * from categoria";
   $resp_categoria = $conexion->query($sql1);
 //Sub categorias
   $sql1 = "select  * from categoria c, subcategoria sc where c.id=sc.id_categoria ";
   $resp_subcat = $conexion->query($sql1);

  // marca
  $sql2 = "select  * from marca";
      $resp_marca = $conexion->query($sql2); 

?>
<?php
    if (isset($_FILES['attachments'])) {
        $msg = "";
        $targetFile = "../assets/img/productos/" . basename($_FILES['attachments']['name'][0]);
        if (file_exists($targetFile))
            $msg = array("status" => 0, "msg" => "¡El archivo ya existe!");
        else if (move_uploaded_file($_FILES['attachments']['tmp_name'][0], $targetFile))
            $msg = array("status" => 1, "msg" => "El archivo ha sido subido", "path" => $targetFile);

        exit(json_encode($msg));
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons 
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- funciones -->
<script src="funciones/modulo-producto.js"></script>
<script src="../assets/js/jquery-2.2.4.min.js"></script>

<script language="javascript">
      $(document).ready(function(){
        $("#categoria").change(function () {

          //$('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
          
          $("#categoria option:selected").each(function () {
            id_estado = $(this).val();
            $.post("procesos/get-subcategoria.php", { id_estado: id_estado }, function(data){
              $("#subcategoria").html(data);
            });            
          });
        })
      });
      
    </script>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar producto</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div role="form">
                <div class="card-body">
                  <div class="row">
                    
                    <div class="col-sm-6 ">
                       <div class="form-group ">
                        <label for="example-search-input" class="col-12 col-form-label">Imagen:</label>
                        <center>
                          <div id="dropZone">
                            <div id="files"> </div>

                            <div class="form-group ">
                              <input type="text" id="archivo1" name="archivo1" disabled="disabled" >
                            </div>
                            <input class="form-control" type="file" id="fileupload" name="attachments[]" multiple>
                          </div>
                          <h1 id="error"></h1>
                          <br>
                          <h1 id="progress"></h1>
                          <br>

                        </center>
                      </div>
                    </div>
                   
                    <div class="col-sm-3 ">
                        <div class="form-group">
                          <label >Seleccionar categoria</label>
                          <select class="form-control" name="categoria" id="categoria">
                          <?php
                            while ($row=$resp_categoria->fetch_array()) 
                              {
                                echo "<option value='".$row['id']."'>".$row['nombre']."</option>";          
                              }
                          ?>	
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Seleccionar marca</label>
                          <select class="form-control" id="marca">
                          <?php
                            while ($row=$resp_marca->fetch_array()) 
                              {
                                echo "<option value='".$row['id']."'>".$row['nombre']."</option>";          
                              }
                          ?>  
                          </select>
                        </div>
                        
                                 
                    </div>
                    <div class="col-sm-3 ">
                    	<div class="form-group">
                          <label >Seleccionar subcategoria</label>
                          <select class="form-control" name="subcategoria" id="subcategoria">
                          
                          </select>
                        </div>
                        <div class="form-group">
                          <label >Nombre producto</label>
                          <input type="text" class="form-control" id="nombre">
                        </div>
                        
                        
                    </div>
                </div>
                  <div class="row">
                      <div class="col-sm-2 ">
                      <div class="form-group">
                          <label >Presentacion</label>
                          <input type="text" class="form-control" id="presentacion">
                        </div>
                      </div>
                    <div class="col-sm-2 ">
                      <div class="form-group">
                        <label >Precio unidad</label>
                        <input type="text" class="form-control" id="precio_unidad">
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group">
                        <label >Precio por cuarta</label>
                        <input type="text" class="form-control" id="precio_cuarta">
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group">
                        <label >Precio por compra</label>
                        <input type="text" class="form-control" id="precio_compra" >
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group">
                        <label >Cantidad</label>
                        <input type="text" class="form-control" id="cantidad">
                      </div>
                    </div>
                      
                  </div>   


                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="button" class="btn btn-success"  onclick="guardar_producto()">Guardar</button>
                   <button type="button" class="btn btn-primary" onclick="cancelar_funcion_producto()">Cancelar</button>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
     </div>
    </section>
    <!-- /.content -->
    

    
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>
<!-- fileupload -->
    <script src="../assets/js/fileupload/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../assets/js/fileupload/jquery.fileupload.js" type="text/javascript"></script>
    <script src="../assets/js/fileupload/jquery.iframe-transport.js" type="text/javascript"></script>
<script type="text/javascript">
        //imagen 1
        $(function() {
            var files = $("#files");

            $("#fileupload").fileupload({
                url: 'registrar-producto.php',
                dropZone: '#dropZone',
                dataType: 'json',
                autoUpload: false
            }).on('fileuploadadd', function(e, data) {
                var fileTypeAllowed = /.\.(gif|jpg|png|jpeg|JPG|PNG|GIF|JPEG)$/i;
                var fileName = data.originalFiles[0]['name'];
                var fileSize = data.originalFiles[0]['size'];
                document.getElementById("archivo1").value = fileName;
                // document.getElementById("archivo2").value = fileName;
                
                //$("#files").fadeIn().append('<p><img style="width: 200px; height: auto;" src="../img/galeria/'+fileName+'" /></p>');

                if (!fileTypeAllowed.test(fileName))
                    $("#error").html('¡Solo se permiten imágenes!');
                else if (fileSize > 500000000000)
                    $("#error").html('¡Tu archivo es demasiado grande! El tamaño máximo permitido es: 500 KB');
                else {
                    $("#error").html("");
                    data.submit();
                }
            }).on('fileuploaddone', function(e, data) {
                var status = data.jqXHR.responseJSON.status;
                var msg = data.jqXHR.responseJSON.msg;

                if (status == 1) {
                    var path = data.jqXHR.responseJSON.path;
                    $("#files").fadeIn().append('<p><img style="width: 200px; height: auto;" src="' + path + '" /></p>');
                } else
                    $("#error").html(msg);
            }).on('fileuploadprogressall', function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#progress").html("Terminado: " + progress + "%");
            });
        });
      
    </script>

</body>
</html>
