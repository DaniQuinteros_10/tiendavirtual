/* ======================================
                CATEGORIA
=========================================*/
function guardar_categoria(){
	var nombre =document.getElementById('nombre').value;
	var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-agregar-categoria.php', {
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
          })
   console.log("reg ok");   
}

function cancelar_funcion_categoria(){
     window.location.replace("lista-categoria.php"); 
}

function form_modificar_categoria(variable){
     window.location.replace("modificar-categoria.php?cat="+variable);
}

function modificar_categoria(){
  var id =document.getElementById('id').value;
  var nombre =document.getElementById('nombre').value;
  var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-modificar-categoria.php', {
              "id": id,
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
            location.replace("lista-categoria.php"); 
          })
   console.log("modificar ok");   
   
}
function modificar_estado_categoria(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-categoria.php?re="+id; 
}
/* ======================================
                SUBCATEGORIA
=========================================*/
function guardar_subcategoria(){
	var categoria =document.getElementById('categoria').value;
    var nombre =document.getElementById('nombre').value;
	var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-agregar-subcategoria.php', {
         "categoria": categoria,     
        "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
          })
   console.log("reg ok");   
}

function cancelar_funcion_subcategoria(){
     window.location.replace("lista-subcategoria.php"); 
}
function modificar_subcategoria(){
  var id =document.getElementById('id').value;
  var categoria =document.getElementById('categoria').value;
    var nombre =document.getElementById('nombre').value;
  var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-modificar-subcategoria.php', {
              "id": id,
        "categoria": categoria,
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
            location.replace("lista-subcategoria.php"); 
          })
   console.log("modificar ok");   
   
}
function form_modificar_subcategoria(variable){
     window.location.replace("modificar-subcategoria.php?subcat="+variable);
}
function modificar_estado_subcategoria(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-subcategoria.phpprocesar-modificar-estado-subcategoria.php?re="+id; 
}

/* ======================================
                MARCA
=========================================*/
function cancelar_funcion_marca(){
     window.location.replace("lista-marcas.php"); 
}
function guardar_marca(){
  var nombre =document.getElementById('nombre').value;
  var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-agregar-marca.php', {
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
            window.location.replace("lista-marcas.php");
          })
   console.log("reg ok");   
    window.location.replace("lista-marcas.php");
}
function form_modificar_marca(variable){
     window.location.replace("modificar-marca.php?ma="+variable);
}
function modificar_marca(){
  var id =document.getElementById('id').value;
  var nombre =document.getElementById('nombre').value;
  var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-modificar-marca.php', {
               "id": id,
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);
                window.location.replace("lista-marcas.php");                           
          })

   console.log("reg ok");   
   
}
function modificar_estado_marca(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-marca.php?re="+id; 
}
/* ======================================
                PRODUCTOS
=========================================*/
function cancelar_funcion_producto(){
     window.location.replace("lista-productos.php"); 
}
function guardar_producto(){
	var categoria =document.getElementById('categoria').value;
    var subcategoria =document.getElementById('subcategoria').value;
	var marca =document.getElementById('marca').value;
	var nombre =document.getElementById('nombre').value;
	var presentacion =document.getElementById('presentacion').value;
	var precio_unidad =document.getElementById('precio_unidad').value;
  var precio_cuarta =document.getElementById('precio_cuarta').value;
  var precio_compra =document.getElementById('precio_compra').value;
  var cantidad =document.getElementById('cantidad').value;
   var archivo1 =document.getElementById('archivo1').value;
    $.post('procesos/procesar-agregar-producto.php', {
              "categoria": categoria,
              "subcategoria": subcategoria,
              "marca": marca,
              "nombre":nombre,
              "presentacion": presentacion,
              "precio_unidad": precio_unidad,
              "precio_cuarta": precio_cuarta,
              "precio_compra": precio_compra,
              "cantidad": cantidad,
              "archivo1": archivo1                
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
          window.location.replace("lista-productos.php");
          })
   console.log("reg ok");   
    
}
function form_modificar_producto(variable){
     window.location.replace("modificar-producto.php?prod="+variable);
}
function modificar_producto(){
  var id =document.getElementById('id').value;
  var categoria =document.getElementById('categoria').value;
     var subcategoria =document.getElementById('subcategoria').value;
  var marca =document.getElementById('marca').value;
  var nombre =document.getElementById('nombre').value;
  var presentacion =document.getElementById('presentacion').value;
  var precio_unidad =document.getElementById('precio_unidad').value;
  var precio_cuarta =document.getElementById('precio_cuarta').value;
  var precio_compra =document.getElementById('precio_compra').value;
  var cantidad =document.getElementById('cantidad').value;
   var archivo1 =document.getElementById('archivo1').value;
    $.post('procesos/procesar-modificar-producto.php', {
              "id": id,
              "categoria": categoria,
              "subcategoria": subcategoria,
              "marca": marca,
              "nombre":nombre,
              "presentacion": presentacion,
              "precio_unidad": precio_unidad,
              "precio_cuarta": precio_cuarta,
              "precio_compra": precio_compra,
              "cantidad": cantidad,
              "archivo1": archivo1                
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
           window.location.replace("lista-productos.php");
         })
   console.log("reg ok");   
    
}
function modificar_estado_producto(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-producto.php?re="+id; 
}

//ELIMINAR sin elimiar de base de datos, solo cambiar el estado para no perder el registro de id
function eliminar_producto(a){
  var id=a;
 // alert(b);
 
 var mensaje = confirm("¿Está seguro que desea eliminar este producto?");
    //Detectamos si el usuario acepto el mensaje
    if (mensaje) {
      window.location="procesos/procesar-eliminar-producto.php?re="+id; 
   }
        //Detectamos si el usuario denegó el mensaje
        else {
          alert("¡Haz denegado el mensaje!");
        }

}

/* ======================================
                GALERIA PRODUCTOS
=========================================*/
function cancelar_funcion_galeria_producto(variable){
     window.location.replace("lista-galeria-productos.php?prod="+variable);
}
function form_lista_galeria(variable){
     window.location.replace("lista-galeria-productos.php?prod="+variable);
}
function form_agregar_galeria_producto(producto){
     window.location.replace("registrar-galeria-producto.php?prod="+producto);
}

function registrar_galeria_producto(a){
  var id_producto =document.getElementById('id_producto').value;
  var seo =document.getElementById('seo').value;
  var archivo1 =document.getElementById('archivo1').value;
    $.post('procesos/procesar-agregar-galeria-producto.php', {
              "id_producto": id_producto,
              "seo": seo,
              "archivo1":archivo1              
            },function(data) {
              //console.log('procesamiento finalizado', data);    
            window.location.replace("lista-galeria-productos.php?prod="+a);
          })
   console.log("reg ok");   
   
}
function form_modificar_galeria_producto(a,b){
     window.location.replace("modificar-galeria-producto.php?gal="+a+"&prod="+b);
}

function modificar_galeria_producto(a){
  var id =document.getElementById('id').value;
  var seo =document.getElementById('seo').value;
  var archivo1 =document.getElementById('archivo1').value;
    $.post('procesos/procesar-modificar-galeria-producto.php', {
              "id": id,
              "seo": seo,
              "archivo1":archivo1              
            },function(data) {
              //console.log('procesamiento finalizado', data);    
            window.location.replace("lista-galeria-productos.php?prod="+a);
          })
   console.log("reg ok");   
   
}
function modificar_estado_galeria_producto(a, b){
 var id=a;
  var p=b;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-galeria-producto.php?re="+id+"&prod="+p; 
}

//ELIMINAR desde base de datos
function eliminar_galeria_producto(a,b){
  var gal=a;
  var prod=b;
 
 var mensaje = confirm("¿Está seguro que desea eliminar este registro?");
    //Detectamos si el usuario acepto el mensaje
    if (mensaje) {
      window.location="procesos/procesar-eliminar-galeria-producto.php?gal="+a+"&prod="+b;
   }
        //Detectamos si el usuario denegó el mensaje
        else {
          alert("¡Haz denegado el mensaje!");
        }

}