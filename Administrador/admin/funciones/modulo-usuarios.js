/* ======================================
                ROLES
=========================================*/
function cancelar_funcion_rol(){
     window.location.replace("lista-roles.php"); 
}
function registrar_rol(){
	var nombre =document.getElementById('nombre').value;
	var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-agregar-rol.php', {
              "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);  
         window.location.replace("lista-roles.php"); 
          })
   console.log("reg ok");   
}

function form_modificar_rol(variable){
    window.location.replace("modificar-rol.php?rol="+variable);
}
function modificar_rol(){
	var id =document.getElementById('id').value;
    var nombre =document.getElementById('nombre').value;
	var descripcion =document.getElementById('descripcion').value;
    $.post('procesos/procesar-modificar-rol.php', {
                "id": id,      
                "nombre": nombre,
              "descripcion":descripcion              
            },function(data) {
              //console.log('procesamiento finalizado', data);  
         window.location.replace("lista-roles.php"); 
          })
   console.log("reg ok");   
}
/* ======================================
                USUARIOS
=========================================*/
function cancelar_funcion(){
     window.location.replace("lista-usuarios.php"); 
}

function guardar_usuario(){
	var nombre =document.getElementById('nombre').value;
  var apellido =document.getElementById('apellido').value;
  var email =document.getElementById('email').value;
  var tipo_usuario =document.getElementById('tipo_usuario').value;
  var nombre_usuario =document.getElementById('nombre_usuario').value;
	var password =document.getElementById('password').value;
    $.post('procesos/procesar-agregar-usuario.php', {
              "nombre": nombre,
              "apellido":apellido,         
              "email":email,              
               
              "tipo_usuario":tipo_usuario,             
              "nombre_usuario":nombre_usuario,              
              "password":password     
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
           window.location.replace("lista-usuarios.php"); 
          })
   console.log("reg de usuario ok");  
  
}
function form_modificar_usuario(variable){
    window.location.replace("modificar-usuario.php?user="+variable);
}

function modificar_usuario(){
  var id =document.getElementById('id').value;
  
  var nombre =document.getElementById('nombre').value;
  var apellido =document.getElementById('apellido').value;
  var email =document.getElementById('email').value;
  var tipo_usuario =document.getElementById('tipo_usuario').value;
  var nombre_usuario =document.getElementById('nombre_usuario').value;
  var password =document.getElementById('password').value;
    $.post('procesos/procesar-modificar-usuario.php', {
              "id": id,
              "nombre": nombre,
              "apellido":apellido,         
              "email":email,              
               
              "tipo_usuario":tipo_usuario,             
              "nombre_usuario":nombre_usuario,              
              "password":password     
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
          window.location.replace("lista-usuarios.php"); 
          })
   console.log("mod de usuario ok");  
    
}
//ELIMINAR sin elimiar de base de datos, solo cambiar el estado para no perder el registro de id
function eliminar_usuario(a){
  var id=a;
 // alert(b);
 
 var mensaje = confirm("¿Está seguro que desea eliminar este usuario?");
    //Detectamos si el usuario acepto el mensaje
    if (mensaje) {
      window.location="procesos/procesar-eliminar-usuario.php?re="+id; 
   }
        //Detectamos si el usuario denegó el mensaje
        else {
          alert("¡Haz denegado el mensaje!");
        }

}
function modificar_estado_usuario(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-usuario.php?re="+id; 
}

/* ======================================
                CLIENTES
=========================================*/
function cancelar_funcion_cliente(){
     window.location.replace("lista-clientes.php"); 
}

function form_modificar_cliente(variable){
    window.location.replace("modificar-cliente.php?cli="+variable);
}
function guardar_cliente(){
	var nombre =document.getElementById('nombre').value;
  var apellido =document.getElementById('apellido').value;
  var email =document.getElementById('email').value;
 
  var tipo_usuario =document.getElementById('tipo_usuario').value;
  var nombre_usuario =document.getElementById('nombre_usuario').value;
	var password =document.getElementById('password').value;
    $.post('procesos/procesar-agregar-cliente.php', {
              "nombre": nombre,
              "apellido":apellido,         
              "email":email,               
              "tipo_usuario":tipo_usuario,             
              "nombre_usuario":nombre_usuario,              
              "password":password     
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
           window.location.replace("lista-clientes.php"); 
          })
   console.log("reg de usuario ok");  
  
}


function modificar_cliente(){
  var id =document.getElementById('id').value;
  
  var nombre =document.getElementById('nombre').value;
  var apellido =document.getElementById('apellido').value;
  var email =document.getElementById('email').value;
 
  var tipo_usuario =document.getElementById('tipo_usuario').value;
  var nombre_usuario =document.getElementById('nombre_usuario').value;
  var password =document.getElementById('password').value;
    $.post('procesos/procesar-modificar-cliente.php', {
              "id": id,
              "nombre": nombre,
              "apellido":apellido,         
              "email":email,              
              "tipo_usuario":tipo_usuario,              
                         
              "nombre_usuario":nombre_usuario,          
              "password":password     
            },function(data) {
              //console.log('procesamiento finalizado', data);                           
          window.location.replace("lista-clientes.php"); 
          })
   console.log("mod de usuario ok");  
    
}
//ELIMINAR sin elimiar de base de datos, solo cambiar el estado para no perder el registro de id
function eliminar_cliente(a){
  var id=a;
 // alert(b);
 
 var mensaje = confirm("¿Está seguro que desea eliminar este cliente?");
    //Detectamos si el usuario acepto el mensaje
    if (mensaje) {
      window.location="procesos/procesar-eliminar-cliente.php?re="+id; 
   }
        //Detectamos si el usuario denegó el mensaje
        else {
          alert("¡Haz denegado el mensaje!");
        }

}
function modificar_estado_cliente(a){
 var id=a;
 // alert(b);
 window.location="procesos/procesar-modificar-estado-cliente.php?re="+id; 
}
function form_modificar_info_entrefa_cliente(variable){
    window.location.replace("modificar-cliente.php?cli="+variable);
}