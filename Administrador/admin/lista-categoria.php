<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- DataTables -->
  <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- funciones -->
  <script type="text/javascript" src="funciones/modulo-producto.js"></script>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <section class="content ">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista categoria</h3> 
            </div>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th>Nombre categoria</th>
                 <th>Descripcion</th>
                  <th>Acciones</th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $query="SELECT nombre, descripcion, id, estado FROM categoria  "; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                  {
                    ?>    
                <tr>
                 
                 
                  <td><?php echo $row['nombre'];?></td>
                <td><?php echo $row['descripcion'];?></td>
                  <td>
                   <div class="btn-group">
                     <button class="btn btn-default" type="button" onclick="form_modificar_categoria(<?php echo $row['id'];?>)"><i class="fas fa-pencil-alt mr-2"></i>modificar</button>
                     <button class="btn btn-default" type="button" onclick="modificar_estado_categoria(<?php echo $row['id'];?>)"><i class="fas fa-eye mr-2"></i><?php echo $row['estado'];?></button>
                  
                   </div>
                  </td>
                 
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </section>

    
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>

<!-- DataTables -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
