<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- DataTables -->
  <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- funciones -->
  <script type="text/javascript" src="funciones/modulo-ventas.js"></script>
  <script type="text/javascript" src="funciones/modulo-reportes.js"></script>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <section class="content ">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista pedidos</h3> 
            </div>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th>Fecha</th>
                  <th>Cliente</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Finalizar</th>
                  <th>Detalle</th>
                  <th>Ubicacion</th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $mayor_menor=array();
                  $contador_ventas=0; 
                  $query="SELECT venta.id, detalle_entrega.latitud,detalle_entrega.longitud,venta.id_usuario, venta.fecha, sum(detalle_venta.precio)as precio, sum(detalle_venta.cantidad)as cantidad, usuarios.nombre,usuarios.apellido FROM venta join detalle_venta on venta.id=detalle_venta.id_venta JOIN usuarios on venta.id_usuario=usuarios.id join detalle_entrega on venta.id=detalle_entrega.id_venta where venta.estado!='finalizado'  GROUP BY detalle_venta.id_venta  "; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                  {
                    array_push($mayor_menor,$row['precio']);
                    $contador_ventas=$contador_ventas+1;
                    ?>    
                <tr>
                  <td><?php echo $row['fecha'];?></td>
                  <td><?php echo $row['nombre']." ".$row['apellido'];?></td>
                  <td><?php echo $row['cantidad'];?></td>
                  <td><?php echo $row['precio'];?></td>
                  <td>      
                     <button type="button" class="btn btn-default"  onclick="finalizar_pedido(<?php echo $row['id'];?>)">
                          <i class="far fa-file-alt"></i> Finalizar
                        </button>                   
                  </td>
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg" onclick="detalle_compra(<?php echo $row['id'];?>)">
                          <i class="far fa-file-alt"></i> Ver detalle
                        </button>
                  </td>
                  <td>
                     <?php echo "<a href='https://www.google.com/maps?q=".$row['latitud'].",".$row['longitud']."'>"; ?>                    
                       <button type="button" class="btn btn-default">
                          <i class="far fa-file-alt"></i> Ubicacion
                        </button>
                      <?php echo "</a>";?>  
                  </td>
                 
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.card-body -->
            <!--modal-->
               <div class="modal fade" id="modal-lg">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Detalle Venta</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" id="modal">
                      </div>            
                    </div>          
                  </div>
                </div>
              <!--fin modal-->
          </div>
        </div>
      </div>
    </section>

    
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>

<!-- DataTables -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
