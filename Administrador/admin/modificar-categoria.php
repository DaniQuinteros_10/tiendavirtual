<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; ?>
<?php 
$query="SELECT * FROM categoria where  id=$_GET[cat] ";
         $resultado=$conexion->query($query);
         while($row=$resultado->fetch_assoc()){
            $nombre=$row['nombre'];
            $descripcion=$row['descripcion'];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons 
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- funciones -->
   <script type="text/javascript" src="funciones/modulo-producto.js"></script> 
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">modificar categoria</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12 ">
                        <div class="form-group">
                          <label >Nombre categoria</label>
                          <input type="text" class="form-control" id="nombre" value="<?php echo $nombre;?>" >
                        </div>
                   
                        <div class="form-group">
                          <label >Descripcion</label>
                          
                          <textarea class="form-control" id="descripcion"><?php echo $descripcion;?></textarea>
                        </div>

						             
                    </div>
                   </div>
                    <input type="text" class="form-control" id="id" value="<?php echo $_GET['cat'];?>" hidden>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="button" class="btn btn-success" onclick="modificar_categoria()">Guardar</button>
                   <button onclick="cancelar_funcion_categoria()" type="button" class="btn btn-primary" >Cancelar</button>
                </div>
              </form>
              <!-- <div id="registro"></div> -->
            </div>
            <!-- /.card -->
          </div>
        </div>
     </div>
    </section>
    <!-- /.content -->
    

    
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>
<!-- fileupload -->
    <script src="../assets/js/fileupload/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../assets/js/fileupload/jquery.fileupload.js" type="text/javascript"></script>
    <script src="../assets/js/fileupload/jquery.iframe-transport.js" type="text/javascript"></script>


</body>
</html>
