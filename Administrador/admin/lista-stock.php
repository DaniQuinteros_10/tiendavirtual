<?php include "../php/control-sesion.php"; ?>
<?php include "../php/conexion.php"; ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Tienda Online | administrador</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- DataTables -->
  <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<!-- funciones -->
  <script type="text/javascript" src="funciones/modulo-stock.js"></script>
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  	<?php include '../php/menu-superior.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
	<?php include '../php/menu-lateral.php' ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <section class="content ">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista stock</h3> 
            </div>

            <!-- /.card-header -->
            <div class="card-body" id="tablastock">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th>Nombre producto</th>
                 <th>Cantidad vendido</th>
                  <th>Cantidad almacen</th>
                  <th>Total de productos</th>
                  <th>Agregar</th>
                </tr>
                </thead>
                <tbody >
                  <?php 
                  $num_productos_bajos=0;
                  $query="SELECT producto.id,producto.nombre, id_producto,sum(detalle_venta.cantidad)as cantidad_vendidos, producto.cantidad FROM detalle_venta join producto on detalle_venta.id_producto=producto.id GROUP by id_producto"; $resultado=$conexion->query($query); while($row=$resultado->fetch_assoc())
                  {
                    ?>    
                <tr>
                 
                 
                  <td><?php echo $row['nombre'];?></td>
                <td><?php echo $row['cantidad_vendidos'];?></td>
                <td ><?php if($row['cantidad']<=10){$num_productos_bajos=$num_productos_bajos+1; echo "<label style='color:red;'>Quedan ".$row['cantidad']." productos</label>";} else{echo $row['cantidad'];}?></td>
                 <td><?php echo $row['cantidad_vendidos']+$row['cantidad'];?></td>
                 <td>
                
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" onclick="producto(<?php echo $row['id'];?>,)"><i class="fas fa-plus"></i>
                           Detalle del producto
                        </button>
                
                  </td>
                </tr>
                <?php } ?>
              </table>
              <?php echo "<label style='color:red;'>( ".$num_productos_bajos." ) productos tienen una cantidad menor en Almacen</label>";?>
            </div>
            <!-- /.card-body -->
            <!--modal-->
               <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Agregar Cantidad Producto Almacen</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <input type="hidden" id="addproducto"> 
              <div class="row">
              <div class="col-md-2"><label>Cantidad:</label></div>
              <div class="col-md-5"><input  type="number" id="addcantidad" class="form-control"></div>
              <div class="col-md-5"><button type="button" class="btn btn-success" onclick="agregar_producto()">Agregar</button></div>
              </div>
                           
            </div>
            
          </div>
          
        </div>
       
      </div>
              <!--fin modal-->
          </div>
        </div>
      </div>
    </section>

   
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include '../php/footer.php' ?>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../assets/js/adminlte.js"></script>

<!-- DataTables -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
